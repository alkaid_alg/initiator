#!/bin/bash
# author: JiangLinnan
# created: 2022年11月29日
# updated: 2022年11月30日
# 该脚本对基因表型预测pgsnet-beans算法进行调用
# 该脚本接受四个参数
# --input_dir：输入文件
# --output_dir：输出文件存放目录
# --type：预测类型
# --gpus：GPU使用个数

for key in "$@"; do  
  case $key in  
    --input_dir=*)
      input="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output="${key#*=}"
      shift # past argument=value
      ;;
    --type=*)
      type="${key#*=}"
      shift # past argument=value 0 所有表型，1百粒重，2单株荚数，3单株粒数，4底荚高，5株高，6节数，7含油量，8蛋白含量，9总重，10单株粒重
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)  
      ;;  
  esac  
done  
input=${input%*/}
output=${output%*/}
mkdir -p $output
if [ ! -e $input ]; then
	echo "Your input is null, please upload your input correctly"
	exit 1
fi

echo "Your input file is $input"
echo "Your output directory is $output"
echo "The number of GPUs used is $gpus"
echo "The prediction type is $type"

startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
/root/anaconda3/bin/python /predictionphenotype-pgsnet/server.py --input $input --output $output --type $type --gpus $gpus
execute_result=$?
if [ $execute_result -ne 0  ]; then
    echo "Phenotype prediction fails, the result is $execute_result"
    exit $execute_result
fi
echo "Phenotype prediction succeeds, the input dir : $input, you can find the result in the dir: $output"
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete prediction; $startTime ---> $endTime" "Total:$sumTime seconds"
cat $output/result.csv 
