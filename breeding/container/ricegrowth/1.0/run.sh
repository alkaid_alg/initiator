#!/bin/bash
  
# author: Jiaojiao Ye
# created: 2022年11月30日
# 该脚本对ricegrowth水稻成长预测算法进行调用
# 该脚本接受三个参数
# --input: 输入文件
# --ouput: 输出文件存放路径
# --gpus: GPU使用个数

for key in "$@"; do
  case $key in
    --input=*)
      input="${key#*=}"
      shift # past argument=value
      ;;
    --output=*)
      output="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)
      ;;
  esac
done

# 提前创建输出目录
mkdir -p $output
if [ ! -e $input ]; then
	echo "Your input is null, please upload your input correctly"
	exit 1
fi

# 去除shell路径中尾部可能出现的/
echo "Your input file is $input"
echo "Your output dir is $output"
echo "The number of GPUs used is $gpus"

startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`

python /ricegrowth/server.py --input $input  --output $output --gpus $gpus

endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete ricegrowth coverage; $startTime ---> $endTime" "Total:$sumTime seconds"

