#!/bin/bash
# author: yangqinglin
# created: 2022年12月06日10:12:43
# desc: YOLO-F（You only look once-flower）是一种大豆花朵检测的深度学习算法，它能够根据开花期的大豆植株图像检测出大豆植株中的花朵。YOLO是一种通用的目标检测算法，它整合了目标候选区域生成、基础网络目标特征提取、特征提取网络融合、检测目标候选验证这四个阶段，是全新的端到端目标检测算法。YOLO-F采用了模型重参数化方法，设计了一种基于级联的模型缩放策略以及全新的特征提取网络，在推理阶段将多个计算模块合并为一个，针对小目标花朵，在推理阶段将批量归一化的均值和方差整合到卷积层的偏差和权重中，优化检测效果。
# 脚本使用具名参数，约定，需要传入的参数为--input_dir、--output_dir、--gpus,请确保传入有效值。
# --input_dir 为输入图片文件所在目录。
# --output_dir 为输出结果的目录。
# --gpus 为gpus配置

for key in "$@"; do  
  case $key in  
    --input=*)
      input="${key#*=}"
      shift # past argument=value
      ;;
    --output=*)
      output="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)  
      ;;  
  esac  
done  
mkdir -p $output
if [ ! -e $input ]; then
	echo "Your input is null, please upload your input correctly"
	exit 1
fi
echo "Your input file is $input"
echo "Your output dir is $output"
echo "The number of GPUs used is $gpus"
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
gpus=`expr $gpus - 1`
/root/anaconda3/bin/python /flowerdetection/server.py --input $input  --output $output --gpus $gpus
execute_result=$?
if [ $execute_result -ne 0 ]; then
	echo "pod detection failed, the result is $execute_result"
	exit $execute_result
fi
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete prediction; $startTime ---> $endTime" "Total:$sumTime seconds"
