#!/bin/bash

# author: JiangLinnan
# created: 2022年11月29日
# updated: 2022年11月30日
# 该脚本对canopycoverage算法进行调用
# 该脚本接受三个参数
# --input_dir: 输入文件
# --ouput_dir: 输出文件存放路径
# --gpus: GPU使用个数

for key in "$@"; do  
  case $key in  
    --input_dir=*)
      input="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)  
      ;;  
  esac  
done  

input=${input%*/}
output=${output%*/}
mkdir -p output$
if [ ! -e $input ]; then
	echo "Your input directory is null, please upload your input correctly"
	exit 1
fi
echo "Your input directory is $input"
echo "Your output directory is $output"
echo "The number of GPUs used is $gpus"
let gpus--
echo "The max gpu index is $gpus"
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
# python /canopycoverage/server.py --input $input  --output $output --gpus $gpus
/root/anaconda3/bin/python /canopycoverage/server.py --input $input --output $output --gpus $gpus
execute_result=$?
if [ $execute_result -ne 0 ]; then
    echo "Canopy coverage detection fails, the result is $execute_result"
    exit $execute_result
fi
echo "Canopy coverage detection succeeds, the input dir : $input, you can find the result in the dir: $output"
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete canopy coverage; $startTime ---> $endTime" "Total:$sumTime seconds"
cat $output/result.csv
