#!/bin/bash
# date: 2022年12月5日14:53:05
# author: hepengfei
# call: bash run.sh --input_dir=/root/testdata --output_dir=/root/result --gpus=0
# desc： 豆荚检测的镜像启动脚本，用于启动豆荚检测，描绘豆粒数，豆荚等信息图像
# 注: --gpus 的值一定要小于pod能够使用的gpu卡数，不等于分配的卡的数量。

for key in "$@"; do  
  case $key in  
    --input_dir=*)
      input="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)  
      ;;  
  esac  
done  
mkdir -p $output
if [ ! -e $input ]; then
	echo "Your input is null, please upload your input correctly"
	exit 1
fi

echo "Your input file is $input"
echo "Your output dir is $output"
echo "The number of GPUs used is $gpus"

echo $PATH
export PATH="/root/anaconda3/bin:$PATH"
#sleep 1d
echo $PATH
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
gpus=`expr $gpus - 1`
python /poddetection/server.py --input $input  --output $output --gpus $gpus
execute_result=$?
if [ $execute_result -ne 0 ]; then
	echo "pod detection failed, the result is $execute_result"
	exit $execute_result
fi
echo "pod detection success, the input dir : $input, you can find the result in the dir: $output" 
wait
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete poddetection; $startTime ---> $endTime" "Total:$sumTime seconds"
