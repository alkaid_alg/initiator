#!/bin/bash
# 2022年12月9日17:38:38
# author: hepengfei
# param：注： --gpus为卡数，在使用时需要-1
# call example: bash run.sh --input_dir /root/input --output_dir /root/result --gpus=1

for key in "$@"; do  
  case $key in  
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    *)  
      ;;  
  esac  
done  
mkdir -p $output_dir
if [ ! -e $input_dir ]; then
	echo "Your input is null, please upload your input correctly"
	exit 1
fi

echo "Your input file is $input_dir"
echo "Your output dir is $output_dir"
echo "The number of GPUs used is $gpus"
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
gpus=`expr $gpus - 1`
/root/anaconda3/bin/python /breedidentification/server.py --input $input_dir  --output $output_dir --gpus $gpus
execute_result=$?
if [ $execute_result -ne 0 ]; then
	echo "breedidentification failed, the result is $execute_result"
	exit $execute_result
fi
echo "breedidentification success, the input dir : $input_dir, you can find the result in the dir: $output_dir" 

endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]
echo "Complete prediction; $startTime ---> $endTime" "Total:$sumTime seconds"
