#!/bin/bash

for key in "$@"; do
  case $key in  
    --input_dir=*)  
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value 
      ;; 
    *)  
      ;;  
  esac  
done

python period-ls.py --input_path $input_dir --output_path $output_dir


