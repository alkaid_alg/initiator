#! /bin/bash

# author: chensheng
# created: 2022.11.08
# desc: This script is used to execute a single term program

for key in "$@"; do
  case $key in
    --csv_file=*)
      csv_file="${key#*=}"
      shift # past argument=value  
      ;;
    --conf_file=*)
      conf_file="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --other_param=*)
      other_param="${key#*=}"
      shift # past argument=value  
      ;;
    *)
      ;;
  esac
done

if [ -z $csv_file ] ; then
  echo "please input valid .csv data file."
  exit 1
fi

if [ -z $conf_file ] ; then
  echo "please input valid configure file."
  exit 1
fi

if [ -z $output_dir ] || [ ! -d $output_dir ]; then
  echo "please input valid output directory"
  exit 1
fi

mkdir -p $output_dir

# truncate the .csv file name and input directory from the absolute path of .csv file
csv_file_name=${csv_file##*/}
conf_file_name=${conf_file##*/}
input_dir=${csv_file%/*}

cd $input_dir

julia -p  8\
 -E "@everywhere using Pkg; @everywhere Pkg.activate(\"/dros/common/ecosystem/MATERIAL_COMPUTING/apps/Termin/PROGRAM\"); @everywhere using Termin;\
   termin(\"$csv_file_name\",\"$conf_file_name\";plt=true)"\
   >  conf_$(date +%Y%m%d%H%M).log

mv $(ls | egrep "nodimless_|RecommendedFuncconf@|conf_|termfrequency|func_|conf@") $output_dir

echo "complete the run"

