#!/bin/bash  

# author: songquanheng
# created: 2022年9月6日15:03:12
# desc: 该框架运行脚本用于处理comsol运行。脚本具有批量处理能力，会同时处理输入目录下的所有.mph文件
# 
# 脚本使用具名参数，约定，需要传入的参数为--input_dir和--output_dir,请确保传入有效值。
# --input_dir 为mph文件所在目录。
# --output_dir 为输出结果的目录。


# 三个参数
# $1: mph文件的名称 形如blasting_rock.mph
# $2: 输入路径 /mnt/ecosystem/materials/comsol/comsol_job9/data
# $3: 输出路径 /mnt/ecosystem/materials/comsol/comsol_job9/output
run_for_mph() {
  now=$(date "+%Y%m%d-%H%M%S")
  # 获取mph的文件名blasting_rock.mph
  mph_file=$1
  input_dir=$2
  output_dir=$3

  file_name=${mph_file##*/}
  # 取出mph的绝对名称: blasting_rock
  mph_abs_name=${file_name%.*} 

  batch_log_file=$output_dir/$mph_abs_name-$now-$default_batch_log_file
  output_model_file=$output_dir/$mph_abs_name-$now-$default_model_file

  /hpc/software/comsol60/multiphysics/bin/comsol batch -inputfile $mph_file -outputfile ${output_model_file} -batchlog ${batch_log_file} 
  wait
}


default_batch_log_file='batch-log.out'
default_model_file='model.out'

for key in "$@"; do
  case $key in  
    --input_dir=*)  
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --default)  
      DEFAULT=YES  
      shift # past argument with no value  
      ;;  
    *)  
      ;;  
  esac  
done


if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing mph file!"
  exit 1
fi

if [ -z $output_dir ]; then
  echo "please input valid output_dir!"
  exit 1
fi

# 去除shell路径中尾部可能出现的/
input_dir=${input_dir%*/}
output_dir=${output_dir%*/}
mkdir -p $output_dir

# 确保input_dir下有.mph文件
for mph_file in $input_dir/*.mph
do
  echo "Using comsol run the file: " $mph_file
  startTime_s=`date +%s`
  startTime=`date +%Y%m%d-%H:%M:%S`
  
  run_for_mph $mph_file $input_dir $output_dir
  
  endTime_s=`date +%s`
  endTime=`date +%Y%m%d-%H:%M:%S`
  sumTime=$[ $endTime_s - $startTime_s ]

echo "complete handling the mph file: $mph_file; $startTime ---> $endTime" "Total:$sumTime seconds"
done
echo "complete the run."
