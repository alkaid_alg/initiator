#/bin/bash
for key in "$@"; do
  case $key in
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --log_dir=*)
          log_dir="${key#*=}"
          shift
          ;;
    --default)
      ;;
    *)
      ;;
  esac
done

input_dir=${input_dir%*/}
output_dir=${output_dir%*/}
log_dir=${log_dir%*/}
if [ -d $output_dir ];then
  /hpc/software/BIOVIA/MaterialsStudio22.1/etc/CASTEP/bin/RunCASTEP.sh -np 2 $input_dir/BN
else
  mkdir -p $output_dir
  /hpc/software/BIOVIA/MaterialsStudio22.1/etc/CASTEP/bin/RunCASTEP.sh -np 2 $input_dir/BN
fi
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $input_dir
echo $output_dir
echo $script_dir
#log_dir=${2:10}
pwd
cd $script_dir
ls $script_dir| egrep -v "*.sh|*.log" |xargs -i mv {} $output_dir
echo "Complete"
#mv $script_dir/*.log $log_dir
