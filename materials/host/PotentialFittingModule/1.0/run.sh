# !/bin/bash

# author: fukejie
# created: 2022年9月13日14:39:15
# updated: 2022年9月13日14:39:15
# desc: 该脚本用于PotentialFittingModule框架运行脚本
# 该脚本接收两个参数
# --input_dir start_point.mat和input.ini所在目录
# --output_dir 结果输出目录

for key in "$@"; do
  case $key in
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --config_file=*)
          config_file="${key#*=}"
      shift # past argument=value 
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value  
      ;;
    *)
      ;;
  esac
done

# 导入依赖
echo "------------------------------------------"
echo Setting up environment variables
MCRROOT=/hpc/software/PotentialFittingModule/matlab_runtime
echo "------------------------------------------"
LD_LIBRARY_PATH=.:${MCRROOT}/v90/runtime/glnxa64 ;
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/v90/bin/glnxa64 ;
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/v90/sys/os/glnxa64;
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/v90/sys/opengl/lib/glnxa64;
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/usr/lib64/;

export LD_LIBRARY_PATH;

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing start_point.mat and input.ini!"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

input_dir=${input_dir%*/}
output_dir=${output_dir%*/}
mkdir -p $output_dir

cd $input_dir

if [ ! -f "input.ini" ]; then
  echo "please input data directory containing input.ini!"
  exit 1
fi

if [ ! -f "start_point.mat" ]; then
  echo "please input data directory containing start_point.mat!"
  exit 1
fi

cp /hpc/software/PotentialFittingModule/program/A_Fitting A_Fitting

./A_Fitting

echo "finish"

mv o* $output_dir
mv O* $output_dir
rm empty*
rm Restart*
rm A_Fitting
