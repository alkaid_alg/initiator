#!/bin/bash
# author: yangqinglin
# created: 2022年11月30日10:53:41
# desc: 该应用为Gaussian16的科学计算应用。输入文件为包含化合物化学分子式的多行ASCII文本文件
#
# 脚本使用具名参数，约定，需要传入的参数为--molecular_structure_file和--output_dir,请确保传入有效值。
# --molecular_structure_file 为化学分子式文件。
# --output_dir 为输出结果的目录。

for i in /etc/profile.d/*.sh /etc/profile.d/sh.local ; do
    if [ -r "$i" ]; then
        if [ "${-#*i}" != "$-" ]; then
            . "$i"
        else
            . "$i" >/dev/null
        fi
    fi
done

for key in "$@"; do
  case $key in
    --molecular_structure_file=*)
      molecular_structure_file="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
  esac
done

if [ -z $output_dir ] ; then
  echo "please input valid output_dir directory!"
  exit 1
fi


##########加载Gaussian 16###############
export g16root=/hpc/software
export GAUSS_EXEDIR=$g16root/g16
# export GAUSS_SCRDIR=$HOME/tmp
export PATH=$PATH:$g16root/g16
source $g16root/g16/bsd/g16.profile
cd /tmp

# 启动程序
g16 $molecular_structure_file $output_dir
