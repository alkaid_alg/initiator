# !/bin/bash

# author: pengshuang
# created: 2022年11月28日
# desc: 该框架运行脚本用于处理QuantumEspresso运行
# 
# 脚本使用具名参数，约定，需要传入的参数为--input_dir和--output_dir，请确保传入有效值。
# --input_dir 为输入文件所在目录。
# --output_dir 为输出结果的目录。


for key in "$@"; do
  case $key in  
    --input_dir=*)  
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --default)  
      DEFAULT=YES  
      shift # past argument with no value  
      ;;  
    *)  
      ;;  
  esac  
done

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing data.in and *.UPF"
  exit 1
fi

if [ -z $output_dir ]; then
  echo "please input valid output_dir!"
  exit 1
fi

# 去除shell路径中尾部可能出现的/
input_dir=${input_dir%*/}
echo $input_dir
cd $input_dir

# 去除shell路径中尾部可能出现的/
output_dir=${output_dir%*/}
echo $output_dir
input_filename=$(find * -name '*.in')
echo $input_filename

# 提前创建输出目录
mkdir -p $output_dir

# 判断是测试环境还是生产环境
if [ ! -f "/hpc/software/intel/ps2018/parallel_studio_xe_2018/bin/psxevars.sh" ]; then
  export PATH=/hpc/software/intel/Oneapi/intelpython/latest/bin:/usr/local/mpich-3.3.2/bin:$PATH
  export OMPI_ALLOW_RUN_AS_ROOT=1
  export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
  export PATH=$PATH:/hpc/software/qe70/bin
  export LD_LIBRARY_PATH=/hpc/software/intel/Oneapi/mkl/latest/lib/intel64/:$LD_LIBRARY_PATH
else
  source /hpc/software/intel/ps2018/parallel_studio_xe_2018/bin/psxevars.sh intel64
  export LD_LIBRARY_PATH=/hpc/software/lib64:$LD_LIBRARY_PATH
fi

startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
mpirun -np $SLURM_NTASKS /hpc/software/qe70/bin/pw.x < $input_dir/$input_filename |tee data.out

wait
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]

echo "complete handling Quantum Espresso; $startTime ---> $endTime" "Total:$sumTime seconds"

# 将程序输出移到指定的输出目录
mv -f out $output_dir
mv -f data.out $output_dir
