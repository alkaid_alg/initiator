#!/bin/bash

<< BLOCK
参数对应：bash参数 -> python参数
input_dir -> filePath 规定
correlation -> correlation
tolerance_list -> tolerance_list
gpl_dummyfea -> gpl_dummyfea
minsize -> minsize
threshold -> threshold
mininc -> mininc
split_tol -> split_tol
gplearn -> gplearn
population_size -> population_size
generations -> generations
verbose -> verbose
metric -> metric
function_set -> function_set
BLOCK

for key in "$@"; do
  case $key in
    --input_file=*) # it is file_path
      input_file="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --log_dir=*)
      log_dir="${key#*=}"
      shift # past argument=value 
      ;;  
    --correlation=*)
      correlation="${key#*=}"
      shift # past argument=value 
      ;;
    --tolerance_list=*)
      tolerance_list="${key#*=}"
      shift # past argument=value  
      ;;
    --gpl_dummyfea=*)
      gpl_dummyfea="${key#*=}"
      shift # past argument=value  
      ;;
    --minsize=*)
      minsize="${key#*=}"
      shift # past argument=value  
      ;;
    --threshold=*)
      threshold="${key#*=}"
      shift # past argument=value  
      ;;
    --mininc=*)
      mininc="${key#*=}"
      shift # past argument=value  
      ;;
    --split_tol=*)
      split_tol="${key#*=}"
      shift # past argument=value  
      ;;
    --gplearn=*)
      gplearn="${key#*=}"
      shift # past argument=value  
      ;;
    --population_size=*)
      population_size="${key#*=}"
      shift # past argument=value  
      ;;
    --generations=*)
      generations="${key#*=}"
      shift # past argument=value  
      ;; 
    --verbose=*)
      verbose="${key#*=}"
      shift # past argument=value  
      ;; 
    --metric=*)
      metric="${key#*=}"
      shift # past argument=value  
      ;; 
    --function_set=*)
      function_set="${key#*=}"
      shift # past argument=value  
      ;;  
    *)
      ;;
  esac
done

# 判断输入参数值是否存在
if test $input_file
then
  file_path=$input_file
else
  echo "please input the file path"
  exit 1
fi

if test $output_dir
then
  output_dir=$output_dir
else
  echo "please input the output directory"
  exit 1
fi

if test "$correlation"
then
  correlation="$correlation"
else
  correlation="PearsonR(+)"
fi

if test "$tolerance_list"
then
  tolerance_list="$tolerance_list"
else
  tolerance_list="None"
fi

if test "$gpl_dummyfea"
then
  gpl_dummyfea="$gpl_dummyfea"
else
  gpl_dummyfea="None"
fi

if test "$minsize"
then
  minsize="$minsize"
else
  minsize="3"
fi

if test "$threshold"
then
  threshold="$threshold"
else
  threshold="0.95"
fi

if test "$mininc"
then
  mininc="$mininc"
else
  mininc="0.01"
fi

if test "$split_tol"
then
  split_tol="$split_tol"
else
  split_tol="0.8"
fi

if test "$gplearn"
then
  gplearn="$gplearn"
else
  gplearn="False"
fi

if test "$population_size"
then
  population_size="$population_size"
else
  population_size="500"
fi

if test "$generations"
then
  generations="$generations"
else
  generations="100"
fi

if test "$verbose"
then
  verbose="$verbose"
else
  verbose="1"
fi

if test "$metric"
then
  metric="$metric"
else
  metric="mean absolute error"
fi

if test "$function_set"
then
  function_set="$function_set"
else
  function_set="['add', 'sub', 'mul', 'div', 'log', 'sqrt', 'abs', 'neg','inv','sin','cos','tan', 'max', 'min']"
  #function_set="['add']"
fi

# 截取出input_dir
# input_dir=${file_path%/*}
# cd $input_dir

# 输出input directory中的所有项
source_file_list=$(ls $input_dir)
ignore_file_str=""
for item in $source_file_list ;
do
ignore_file_str+="$item|"
done
ignore_file_str=${ignore_file_str%?}

# 去除路径中尾部可能出现的/
output_dir=${output_dir%*/}

# 删除已经存在的Segmented文件夹
if test $output_dir/Segmented
then 
  echo "Deleted the previous folder Segmented and files under the folder Segmented"
  rm -rf $output_dir/Segmented
fi
mkdir -p $output_dir

python startmodel.py --filePath="$file_path" \
                     --correlation="$correlation" \
                     --tolerance_list="$tolerance_list" \
                     --gpl_dummyfea="$gpl_dummyfea" \
                     --minsize="$minsize" \
                     --threshold="$threshold" \
                     --mininc="$mininc" \
                     --split_tol="$split_tol" \
                     --gplearn="$gplearn" \
                     --population_size="$population_size" \
                     --generations="$generations" \
                     --verbose="$verbose" \
                     --metric="$metric" \
                     --function_set="$function_set"

wait
mv $(ls | egrep -v "${ignore_file_str}") $output_dir
wait
mv Result\ of\ TCLR\ * $output_dir
