#!/bin/bash  

# author: luzhengxian
# created: 2022年9月6日19:13
# modify: 2022年9月20日17:53 添加error返回值，从而让平台获得与任务完成情况一致的结果
# desc: 该框架运行脚本用于调用abaqus的作业分析。脚本具有批量处理能力，会同时处理输入目录下的所有.inp文件
# 
# 脚本使用具名参数，约定，需要传入的参数为--input_dir和--output_dir,请确保传入有效值。
# --input_dir 为inp文件所在目录
# --output_dir 为输出结果的目录
##
# 其他用户可选参数说明
# --cpus 为执行分析作业的CPU数量
# --gpus 为执行分析作业的GPU数量
# --args 为其他用户传入Abaqus执行器的变量，例如采用全精度计算时传入"double=both"
##


for key in "$@"; do
    case $key in  
        --input_dir=*)  
            input_dir="${key#*=}"
            shift # past argument=value  
            ;;  
        --output_dir=*)  
            output_dir="${key#*=}"
            shift # past argument=value 
            ;;
        --cpus=*)  
            cpus="${key#*=}"
            shift # past argument=value  
            ;;  
        --gpus=*)  
            gpus="${key#*=}"
            shift # past argument=value  
            ;;  
        --args=*)  
            args="${key#*=}"
            shift # past argument=value  
            ;;  
        *)  
            ;;  
    esac  
done
echo all args: $*


if [ -z $input_dir ] || [ ! -d $input_dir ]; then
    echo "please input data directory containing .inp file!"
    exit 1
fi

if [ -z $output_dir ] || [ ! -d $output_dir ]; then
    echo "please input valid output_dir!"
    exit 1
fi

if [ ! -z $cpus ] && [ $cpus -le 0 ]; then
    echo "cpus must be greater than 0!"
    exit 1
elif [ ! -z $cpus ]; then
    args="$args cpus=$cpus"
fi

if [ ! -z $gpus ] && [ $gpus -lt 0 ]; then
    echo "gpus must be greater than or equal 0!"
    exit 1
elif [ ! -z $gpus ] && [ $gpus -gt 0 ]; then
    args="$args gpus=$gpus"
fi

echo "Run Abaqus executor with args: $args"

# export env
INPUT_DIR=$input_dir
OUTPUT_DIR=$output_dir

# modify: 2022年9月22日10:19 修改工作路径，使用cd进入
# enter the work dir
cd $WORKSPACE

# Run the sh file
./batch_exec_dsls.sh $args

ret_code=$?

echo "complete the job with exit code $ret_code."

exit $ret_code
