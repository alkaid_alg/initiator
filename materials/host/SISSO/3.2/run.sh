#!/bin/bash

# author: sunning
# created: 2022年9月7日16:20:18
# updated: 2022年11月29日18:38:44
# desc: 该脚本用于SISSO框架运行脚本
#       为了同时适应测试环境与线上环境，更新脚本，执行Oneapi的启动脚本
# 该脚本接收两个参数
# --input_dir SISSO.in及train.dat所在目录
# --output_dir 结果输出目录

convex2d_hull="convex2d_hull"
for key in "$@"; do
  case $key in
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value
      ;;
    *)
      ;;
  esac
done

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing SISSO.in and train.dat"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

if [ ! -d $output_dir ] ; then
  mkdir -p $output_dir
fi

input_dir=${input_dir%*/}
if [ ! -f $input_dir/SISSO.in ] ; then
  echo "please input valid SISSO.in!"
  exit 1
fi

if [ ! -f $input_dir/train.dat ] ; then
  echo "please input valid train.dat!"
  exit 1
fi

cd $input_dir
#source /hpc/software/intel/ps2018/parallel_studio_xe_2018/bin/psxevars.sh intel64
#source /hpc/software/intel/ps2019_gcc9/parallel_studio_xe_2019/bin/psxevars.sh intel64
source /hpc/software/intel/Oneapi/setvars.sh &> /dev/null
mpirun -np $SLURM_NTASKS /hpc/software/bin/SISSO_3.2

mv -f SISSO.out $output_dir
mv -f SIS_subspaces $output_dir
mv -f desc_dat $output_dir 
mv -f models $output_dir
if [ -f "$convex2d_hull" ]
then 
    mv $convex2d_hull $output_dir
fi


