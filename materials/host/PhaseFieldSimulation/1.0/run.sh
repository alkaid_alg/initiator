#!/bin/bash


for key in "$@"; do  
  case $key in  
    --calculating_config_file=*)  
      CAL_CONFIG="${key#*=}"  
      shift # past argument=value  
      ;;
    --initial_grid_information_file=*)  
      INIT_GRID="${key#*=}"  
      shift # past argument=value  
      ;;            
    -o=*|--output_dir=*)  
      OUT_DIR="${key#*=}"  
      shift # past argument=value 
      ;;
    -c=*|--cpus=*)  
      N="${key#*=}"  
      shift # past argument=value 
      ;;        
    *)  
      ;;  
  esac  
done  
echo "calculating_config_file  = ${CAL_CONFIG}"  
echo "initial_grid_information_file  = ${INIT_GRID}"  
echo "num_cpu   = ${N}"
echo "output_dir   = ${OUT_DIR}" 

if [ -z $CAL_CONFIG ] || [ -z $INIT_GRID ] || [ -z $OUT_DIR ] ||  [ -z $N ]; then
  echo "use ./run.sh --calculating_config_file=[path of *.in file] --initial_grid_information_file=[path of *.inp file] --output_dir=[dir of output] --cpus=[num of cpu]" 
  exit 1
fi

if  [ ! -d "$OUT_DIR" ]; then
  mkdir ${OUT_DIR}
fi

mv ${INIT_GRID}  /hpc/software/PhaseFieldSimulation/pffe_20211220/program


source /hpc/software/intel/Oneapi/setvars.sh &> /dev/null
export LD_LIBRARY_PATH=/hpc/software/PhaseFieldSimulation/VTK_install_dir/lib64/:/hpc/software/PhaseFieldSimulation/petsc-3.13.4/arch-linux-c-opt/lib/:/hpc/software/mpi-3.3/lib:/hpc/software/PhaseFieldSimulation/libmesh_install_dir/lib:/hpc/software/PhaseFieldSimulation/usr/lib64/lib64:LD_LIBRARY_PATH

cd /hpc/software/PhaseFieldSimulation/pffe_20211220/program
mpirun -n ${N} ./pffe ${CAL_CONFIG}

wait
mv *.vtu *.pvtu *.dat $OUT_DIR

