# !/bin/bash

# author: songquanheng
# created: 2022年9月6日16:19:38
# updated: 2022年11月8日10:49:44
# desc: 该脚本用于vasp框架运行脚本
#       为了适应三个场景，更新脚本，使得对于非静态自洽计算中，5个输入文件可以不被移动
#       把移动时过滤掉的文件从之前写死的"INCAR|POSCAR|POTCAR|KPOINTS",变为自动读取原来的文件内容
# 该脚本接收两个参数
# --input_dir INCAR、POSCAR、POTCAR、KPOINTS所在目录
# --output_dir 结果输出目录

for key in "$@"; do
  case $key in  
    --input_dir=*)  
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --default)  
      DEFAULT=YES  
      shift # past argument with no value  
      ;;  
    *)  
      ;;  
  esac  
done

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing INCAR|POSCAR|POTCAR|KPOINTS!"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

# 去除shell路径中尾部可能出现的/
input_dir=${input_dir%*/}
output_dir=${output_dir%*/}
mkdir -p $output_dir

cd $input_dir
source_file_list=$(ls $input_dir)
ignore_file_str=""
for item in $source_file_list ;
do
ignore_file_str+="$item|"
done
ignore_file_str=${ignore_file_str%?}
echo "ignore_file_str=${ignore_file_str}"
source /hpc/software/intel/Oneapi/setvars.sh &> /dev/null
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
which mpirun
echo $PATH
mpirun /hpc/software/vaspall/vasp.6.3.0/bin/vasp_std
wait
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]

echo "complete handling VASP; $startTime ---> $endTime" "Total:$sumTime seconds"

echo "生成了文件: $(ls | egrep -v "${ignore_file_str}")"
mv $(ls | egrep -v "${ignore_file_str}") $output_dir
