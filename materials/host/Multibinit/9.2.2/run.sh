# !/bin/bash

# author: JiangLinnan
# created: 2022年9月30日
# updated: 2022年11月30日
# desc: 该脚本用于Multibinit运行脚本
# 该脚本接收两个参数
# --input_dir *.in, *.ddb, *.xml, files, jzhang_ece.txt所在目录
# --output_dir 结果输出目录
# module load multibinit/9.2.2
# echo "Applying module multibinit/9.2.2"
# echo "Your mpi is $(which mpirun)"
# echo "Your multibinit is $(which multibinit)"
. /usr/share/Modules/init/bash
module load multibinit/9.2.2
echo "Applying module multibinit/9.2.2"
echo "Your mpi is $(which mpirun)"
echo "Your multibinit is $(which multibinit)"
for key in "$@"; do
  case $key in  
    --input_dir=*)  
      input_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;  
    --default)  
      DEFAULT=YES  
      shift # past argument with no value  
      ;;  
    *)  
      ;;  
  esac  
done

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing *.in|*.ddb or *.xml|*.xml|*.nc"
  exit 1
fi

if [ -z $output_dir ]; then
  echo "please input valid output_dir!"
  exit 1
fi
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/hpc/software/lib64

input_dir=${input_dir%*/}
output_dir=${output_dir%*/}
mkdir -p $output_dir
cd $input_dir

output_name=$(sed -n 2p ./files)
output_name=${output_name%*/}

startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
# /hpc/software/ab_install/bin/mpirun -np $SLURM_NTASKS /hpc/software/abinit-9.2.2/bin/multibinit<files 
mpirun -np $SLURM_NTASKS multibinit<files 
wait
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]

echo "complete multibinit calculation; $startTime ---> $endTime" "Total:$sumTime seconds"
mv $output_name $output_dir
