#!/bin/bash

for key in "$@"; do
  case $key in
    --in_file=*)
      in_file="${key#*=}"
      shift # past argument=value  
      ;;
    --other_files_dir=*)
      other_files_dir="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --other_param=*)
      other_param="${key#*=}"
      shift # past argument=value  
      ;;
    *)
      ;;
  esac
done

if [ -z $in_file ]; then
  echo "please input in file"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

#input_dir=${other_files_dir%/*}
input_dir=$other_files_dir
cd $input_dir
mkdir -p $output_dir

# list the items in the input directory 
source_file_list=$(ls $input_dir)
ignore_file_str=""
for item in $source_file_list ;
do
ignore_file_str+="$item|"
done
ignore_file_str=${ignore_file_str%?}

source /hpc/software/intel/Oneapi/setvars.sh &> /dev/null

# lammps command
# 48
# /hpc/software/intel/Oneapi/mpi/2021.5.1/bin/mpirun -np $SLURM_NTASKS /hpc/software/lammps/lammps-23Jun2022/src/lmp_mpi < $in_file
# aofei
/hpc/software/intel/Oneapi/mpi/2021.5.1/bin/mpirun -np $SLURM_NTASKS /hpc/software/lammps/src/lmp_mpi < $in_file

wait
mv $(ls | egrep -v "${ignore_file_str}") $output_dir

