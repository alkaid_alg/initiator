#!/bin/bash
# author: yejiaojiao
# created: 2022年10月13日14:43
# modyfied:  2022年10月17日， 添加input_dir和output_dir, 支持保存演化计算的历史记录
# modyfied:  2022年11月30日， 删除input_dir, 不需要输入目录
# desc: 该脚本使用abq执行器
#       计算所有文件，并将结果文件放入`$output_dir`中。执行中abq的输出
#       也将同样放入`$output_dir`中，以'<job_name>-log.out'命名
# --output_dir 为输出结果的目录

# params: args: 字符串，表示要传入abq执行器的参数，如"double=both"，选填

# --------- ENV, see also dockerfile ----------
WORKSPACE=/workspace
# ---------------------------------------------

# other arguments to pass to abq executor
args=$1

for key in "$@"; do
  case $key in  
    --output_dir=*)  
      output_dir="${key#*=}"
      shift # past argument=value  
      ;;  
    *)  
      ;;  
  esac  
done


if [ -z $output_dir ] || [ ! -d $output_dir ]; then
    echo "please input valid output_dir!"
    exit 1
fi


# 去除shell路径中尾部可能出现的/
input_dir=${input_dir%*/}
output_dir=${output_dir%*/}

# 提前创建输出目录
mkdir -p $output_dir

# enter the work dir
cd $WORKSPACE

echo "Run Abaqus executor with args: $args"

# Log some info before processing a file
startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`

# -------- Run the abq job executor -----------
/var/DassaultSystemes/SIMULIA/Commands/abq cae noGUI=./def_possion_GA.py

endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]

echo "complete onpr computing; $startTime ---> $endTime" "Total:$sumTime seconds"


exit 0
