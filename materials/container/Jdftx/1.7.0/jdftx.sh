#! /bin/bash
echo "------------------------------------------"
echo Start

export JDFTX_MEMPOOL_SIZE=$3
#export JDFTX_MEMPOOL_SIZE=30000

CUDA_VISIBLE_DEVICES=0  /home/jdftx/jdftx-1.7.0/jdftx/build/jdftx_gpu  -i $1  -o $2
#CUDA_VISIBLE_DEVICES=0  /home/jdftx/jdftx-1.7.0/jdftx/build/jdftx_gpu  -i surface.in  -o  Ag-distance_chargeset.out
#tmux new-session -s Al_s