# !/bin/bash

# author: 梅哲炜
# desc: 该脚本用于使用jdftx软件进行的计算任务
# 该脚本接收6个参数

# --input_dir			输入文件路径
# --output_dir			输出文件路径
# --log_dir				输出日志路径
# --input				输入文件
# --output				输出文件
# --JDFTX_MEMPOOL_SIZE	GPU显存大小


for key in "$@"; do
  case $key in
	
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --log_dir=*)
      log_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --input=*)
      input="${key#*=}"
      shift # past argument=value 
      ;;
    --output=*)
      output="${key#*=}"
      shift # past argument=value 
      ;;
    --JDFTX_MEMPOOL_SIZE=*)
      JDFTX_MEMPOOL_SIZE="${key#*=}"
      shift # past argument=value 
      ;;
    *)
      ;;
  esac
done

echo "------------------------------------------"
echo Task Start

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input valid input directory"
  exit 1
fi

if [ -z $output_dir ] || [ ! -d $output_dir ]; then
  echo "please input valid output directory"
  exit 1
fi

if [ -z $log_dir ] || [ ! -d $log_dir ]; then
  echo "please input valid log directory"
  exit 1
fi

if [ -z $input ] ; then
  echo "please input valid input file."
  exit 1
fi

if [ -z $output ] ; then
  echo "please input valid output file."
  exit 1
fi

if [ -z $JDFTX_MEMPOOL_SIZE ] ; then
  echo "please input valid JDFTX_MEMPOOL_SIZE ."
  exit 1
fi

mkdir /home/task
cp -r $input_dir/*  /home/task
cp jdftx.sh  /home/task
mkdir -p $output_dir

source_file_list=$(ls $input_dir)
ignore_file_str=""
for item in $source_file_list ;
do
ignore_file_str+="$item|"
done
ignore_file_str=${ignore_file_str%?}

cd /home/task

bash  jdftx.sh  $input $output $JDFTX_MEMPOOL_SIZE  &

bash
sleep 30

tail -f --pid=`ps -ef|grep jdftx.sh |grep -v grep|awk '{print $2}'` ./$output
#tail -fn 0 --pid=`ps -ef|grep jdftx.sh |grep -v grep|awk '{print $2}'` ./$output   如果一个容器可在不改变输入文件的情况下，进行多次提交的话，需加-fn 0

wait

mv $(ls | egrep -v "${ignore_file_str}") $output_dir
#mv Ag-dis* $output_dir

echo Task Finish




