# !/bin/bash

# author: fukejie
# created: 2022年12月06日09:40:02
# updated: 2022年12月14日10:22:50
# desc: 该脚本用于ActiveFormationEnergyPrediction框架FeatureEngerning模板运行
# 该脚本接收9个参数
# --SSF                             是否启用多尺度特征工程
# --system_name                     体系名
# --composition                     大尺度特征-化合物组分列表
# --properties                      大尺度特征-体系涉及元素的物理化学性质
# --attributes_of_elements          小尺度特征(需要理解多尺度特征工程)-体系涉及元素的物理化学性质，同properties
# --neighbour_atom                  小尺度特征(需要理解多尺度特征工程)-Voronoi多面体的各种参数，建议理解了Voronoi软件以后进行修改
# --position_info_of_system         小尺度特征(需要理解多尺度特征工程)-每个端际化合物样本根据Wyckoff位置进行的编码
# --sequence                        小尺度特征(需要理解多尺度特征工程)-sigma相端际化合物30个原子对应的Wyckoff位置
# --output_dir                      结果输出目录

#!/bin/bash
for key in "$@"; do
  case $key in
    --SSF=*)
      SSF="${key#*=}"
      shift # past argument=value  
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --system_name=*)
      system_name="${key#*=}"
      shift # past argument=value 
      ;;
    --composition=*)
      composition="${key#*=}"
      shift # past argument=value 
      ;;
    --properties=*)
      properties="${key#*=}"
      shift # past argument=value 
      ;;
    --position_info_of_system=*)
      position_info_of_system="${key#*=}"
      shift # past argument=value 
      ;;
    --attributes_of_elements=*)
      attributes_of_elements="${key#*=}"
      shift # past argument=value 
      ;;
    --sequence=*)
      sequence="${key#*=}"
      shift # past argument=value 
      ;;
    --neighbour_atom=*)
      neighbour_atom="${key#*=}"
      shift # past argument=value 
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value  
      ;;
    *)
      ;;
  esac
done

echo "------------------------------------------"
echo Start

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

SSF=${SSF%*/}
output_dir=${output_dir%*/}
system_name=${system_name%*/}
composition=${composition%*/}
position_info_of_system=${position_info_of_system%*/}
sequence=${sequence%*/}
neighbour_atom=${neighbour_atom%*/}
attributes_of_elements=${attributes_of_elements%*/}


mkdir -p $output_dir

python /ActiveFormationEnergyPrediction/Feature_engineering-1.0/Features_args.py \
--SSF=$SSF \
--attributes_of_elements=$attributes_of_elements \
--composition=$composition \
--neighbour_atom=$neighbour_atom \
--position_info_of_system=$position_info_of_system \
--properties=$properties \
--sequence=$sequence \
--result_dir=$output_dir \
--system_name=$system_name

echo Finish
