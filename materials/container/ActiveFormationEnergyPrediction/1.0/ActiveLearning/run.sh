# !/bin/bash

# author: fukejie
# created: 2022年12月06日09:25:22
# updated: 2022年12月14日10:21:31
# desc: 该脚本用于ActiveFormationEnergyPrediction框架ActiveLearning模板运行
# 该脚本接收11个参数
# --strategy        主动学习策略
# --num_queries     推荐数量
# --setting_value   停止误差
# --iteration       迭代步骤
# --n_jobs          训练线程数
# --system_name     体系名
# --version         version
# --base_set        已打标数据集
# --pool_set        未打标数据集
# --test_set        测试集
# --output_dir      结果输出目录

for key in "$@"; do
  case $key in
    --strategy=*)
      strategy="${key#*=}"
      shift # past argument=value
      ;;
    --num_queries=*)
      num_queries="${key#*=}"
      shift # past argument=value 
      ;;
    --setting_value=*)
      setting_value="${key#*=}"
      shift # past argument=value 
      ;;
    --iteration=*)
      iteration="${key#*=}"
      shift # past argument=value 
      ;;
    --n_jobs=*)
      n_jobs="${key#*=}"
      shift # past argument=value 
      ;;
    --system_name=*)
      system_name="${key#*=}"
      shift # past argument=value 
      ;;
    --version=*)
      version="${key#*=}"
      shift # past argument=value 
      ;;
    --base_set=*)
      base_set="${key#*=}"
      shift # past argument=value 
      ;;
    --pool_set=*)
      pool_set="${key#*=}"
      shift # past argument=value 
      ;;
    --test_set=*)
      test_set="${key#*=}"
      shift # past argument=value 
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value  
      ;;
    *)
      ;;
  esac
done

echo "------------------------------------------"
echo Start

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

strategy=${strategy%*/}
num_queries=${num_queries%*/}
setting_value=${setting_value%*/}
iteration=${iteration%*/}
n_jobs=${n_jobs%*/}
system_name=${system_name%*/}
version=${version%*/}
base_set=${base_set%*/}
pool_set=${pool_set%*/}
test_set=${test_set%*/}
output_dir=${output_dir%*/}

mkdir -p $output_dir

cd $output_dir

python /ActiveFormationEnergyPrediction/ActiveLearning-1.0/main/AL_main_arg.py \
--strategy=$strategy \
--n_jobs=$n_jobs \
--num_queries=$num_queries \
--setting_value=$setting_value \
--iteration=$iteration \
--n_jobs=$n_jobs \
--system_name=$system_name \
--version=$version \
--base_set=$base_set \
--pool_set=$pool_set \
--result_dir=$output_dir \
--test_set=$test_set \
--result_dir=$output_dir/result_dir \
--model_dir=$output_dir/model_dir

echo Finish