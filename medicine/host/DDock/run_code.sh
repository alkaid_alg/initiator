#!/bin/bash
# author: yangqinglin
# created: 2022年11月30日11:09:21
# desc: 目前只能固定测试集计算，暂时无法针对不同输入数据计算。
#
# 脚本使用具名参数，约定，需要传入的参数为--output_dir,请确保传入有效值。
# --output_dir 为输出结果的目录。

for key in "$@"; do
  case $key in
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
  esac
done


if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

# 启动脚本
/root/miniconda3/envs/edock_env/bin/python /root/EDock/generate_pose.py > $output_dir/outcome.txt
echo "Programme is success"