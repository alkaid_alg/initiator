#!/bin/bash
# author: yangqinglin
# created: 2022年11月30日10:14:44
# desc: 该脚本应用于PBSA表单提交模式，需要输入三个文件，通过PBSA计算得到最后输出
#
# 脚本使用具名参数，约定，需要传入的参数为--receptor_protein、--ligand_protein、--coincident_trajectories和--output_dir,请确保传入有效值。
# --receptor_protein 为PBSA需要使用的受体蛋白数据。
# --ligand_protein 为PBSA需要使用的配体蛋白数据
# --coincident_trajectories 为PBSA需要计算的受体蛋白和配体蛋白的重合轨迹
# --output_dir 为输出结果的目录。
for key in "$@"; do
  case $key in
    --receptor_protein=*)
      receptor_protein="${key#*=}"
      shift # past argument=value
      ;;
    --ligand_protein=*)
      ligand_protein="${key#*=}"
      shift # past argument=value
      ;;
    --coincident_trajectories=*)
      coincident_trajectories="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
  esac
done

# 输出目录校验
if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

# 创建输入文件目录
mkdir /tmp/input_dir
input_dir=/tmp/input_dir

# 将输入文件传入输入文件目录
echo "++++++"
cp  $receptor_protein /tmp/input_dir/
cp  $ligand_protein /tmp/input_dir/
cp  $coincident_trajectories /tmp/input_dir/
ls /tmp
ls -al /tmp/input_dir
echo "+++++++"

# 获取输入文件头参数
index=`ls $input_dir | grep pro.gas.prmtop | awk -F "_" '{print $1}'`
index_lig=`ls $input_dir | grep lig.gas.prmtop | awk -F "_" '{print $1}'`
index_nc=`ls $input_dir | grep com.gas.nc | awk -F "_" '{print $1}'`
if [ $index != $index_lig ] || [ $index != $index_nc ];then
	echo "the pro.gas.prmtop/lig.gas.prmtop/com.gas.nc file must have the same index. such as 10gs_pro.gas.prmtop/10gs_lig.gas.prmtop/10gs_com.gas.nc!"
	exit 1
fi

# 加载容器中环境变量，启动conda环境
source /root/.bashrc
conda activate python3.7
cd /mmpbsa/src_VD114

# 运行启动脚本
source run_VD114-MMGBSA.sh $index $input_dir

# 将运行结果保存到输出目录
cp $input_dir/FINAL_RESULTS_MMPBSA.dat $output_dir
rm -rf $input_dir
echo "after commputing"
ls /tmp