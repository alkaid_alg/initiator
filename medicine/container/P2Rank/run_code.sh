#!/bin/bash

# author: fangfang
# created: 2022年12月6日
# updated:
# desc: 该脚本用于P2Rank推理作业运行脚本
#       所用数据集路径为启pod时默认挂的oss路径

# 该脚本接收2个参数
# --input_dir 训练数据集所在文件夹，若不指定则默认使用已挂载数据集
# --output_dir 结果输出目录

export  input_dir="/dros/common/ecosystem/MEDICINE_COMPUTING/apps/P2Rank/EXAMPLE/"

ln -s $input_dir /root/data

sh /root/pocket_demo.sh