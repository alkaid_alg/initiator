#!/bin/bash

# author: fangfang
# created: 2022年12月6日
# updated:
# desc: 该脚本用于P2Rank推理作业运行脚本
#       所用数据集路径为启pod时默认挂的oss路径

# 该脚本接收2个参数
# --input_dir 训练数据集所在文件夹，若不指定则默认使用已挂载数据集
# --output_dir 结果输出目录


for key in "$@"; do
  case $key in
  --input_dir=*)
    input_dir="${key#*=}"
    shift # past argument=value
    ;;
  --output_dir=*)
    output_dir="${key#*=}"
    shift # past argument=value
    ;;
  --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
  *) ;;

  esac
done

input_dir=${input_dir%*/}

ln -s $input_dir /root/data

sh /root/pocket_demo.sh