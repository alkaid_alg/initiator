#!/bin/sh

# author: fangfang
# created: 2022年12月6日
# updated:
# desc: 该脚本用于AutoDock推理作业运行脚本
#       所用数据集路径为启pod时默认挂的oss路径

# 该脚本接收11个参数
# --ligand_path 配体文件地址
# --receptor_path 受体文件地址
# --output_dir 结果输出目录
# --cpus 该作业使用的cpu卡数
# --exhaustiveness 搜索次数
# --center_x 口袋的中心坐标的x
# --center_y 口袋的中心坐标的y
# --center_z 口袋的中心坐标的z
# --size_x 盒子的格点大小x
# --size_y 盒子的格点大小y
# --size_z 盒子的格点大小z

for key in "$@"; do
  case $key in
    --ligand_path=*)
      ligand_path="${key#*=}"
      shift # past argument=value
      ;;
    --receptor_path=*)
      receptor_path="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
    --cpus=*)
      cpus="${key#*=}"
      shift # past argument=value
      ;;
    --exhaustiveness=*)
      exhaustiveness="${key#*=}"
      shift # past argument=value
      ;;
    --center_x=*)
      center_x="${key#*=}"
      shift # past argument=value
      ;;
    --center_y=*)
      center_y="${key#*=}"
      shift # past argument=value
      ;;
    --center_z=*)
      center_z="${key#*=}"
      shift # past argument=value
      ;;
    --size_x=*)
      size_x="${key#*=}"
      shift # past argument=value
      ;;
    --size_y=*)
      size_y="${key#*=}"
      shift # past argument=value
      ;;
    --size_z=*)
      size_z="${key#*=}"
      shift # past argument=value
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value
      ;;
    *)
      ;;
  esac
done

output_dir=${output_dir%*/}
config_path="$output_dir/config.txt"
touch $config_path
echo "ligand = $ligand_path" > $config_path
echo "receptor = $receptor_path" >> $config_path
echo "out = $output_dir/output.pdbq" >> $config_path
echo "exhaustiveness = $exhaustiveness" >> $config_path
echo "center_x = $center_x" >> $config_path
echo "center_y = $center_y" >> $config_path
echo "center_z = $center_z" >> $config_path
echo "size_x = $size_x" >> $config_path
echo "size_y = $size_y" >> $config_path
echo "size_z = $size_z" >> $config_path
echo "cpu = $cpus" >> $config_path

/virtualflow/bin/qvina_w --config $config_path
