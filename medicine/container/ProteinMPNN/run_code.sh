#!/bin/bash

# author: sunning
# created: 2022年10月20日
# updated: 2022年11月30日
# desc: 该脚本用于蛋白质设计ProteinMPNN AI推理作业的运行脚本
#       该脚本是用于计算制药平台前台代码模式的运行
#       运行该脚本需要对对应的/workspace/protein_design路径进行挂载
# 该脚本接收两个参数
# --pdb_code 蛋白质结构代码
# --output_dir 结果输出目录
source activate PD

cd /workspace/protein_design
mkdir -p input
python /workspace/protein_design/main.py \
	--pdb_code=1O91 \
	--output_dir=$output_dir
 
conda deactivate
