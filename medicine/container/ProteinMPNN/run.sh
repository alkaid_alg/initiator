#!/bin/bash

# author: sunning
# created: 2022年10月20日
# updated: 2022年11月30日
# desc: 该脚本用于蛋白质设计ProteinMPNN AI推理作业的运行脚本
#       运行该脚本需要对对应的/workspace/protein_design路径进行挂载
# 该脚本接收两个参数
# --pdb_code 蛋白质结构代码
# --output_dir 结果输出目录

source activate PD

for key in "$@"; do
  case $key in
    --pdb_code=*)
      pdb_code="${key#*=}"
      shift # past argument=value  
      ;;
    # --input_dir=*)
    #   input_dir="${key#*=}"
    #   shift # past argument=value 
    #   ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value 
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value  
      ;;
    *)
      ;;
  esac
done

pdb_code=${pdb_code%*/}
# input_dir=${input_dir%*/}
# mkdir -p $input_dir
output_dir=${output_dir%*/}
mkdir -p $output_dir
cd /workspace/protein_design


startTime_s=`date +%s`
startTime=`date +%Y%m%d-%H:%M:%S`
python /workspace/protein_design/main.py \
	--pdb_code=$pdb_code \
	--output_dir=$output_dir
	# --input_dir=$input_dir \
wait
endTime_s=`date +%s`
endTime=`date +%Y%m%d-%H:%M:%S`
sumTime=$[ $endTime_s - $startTime_s ]

echo "The pdb_code is: $pdb_code"
echo "The output directory is: $output_dir"
echo "complete protein design using: $sumTime seconds"

conda deactivate
