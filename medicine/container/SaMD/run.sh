# !/bin/bash

# author: pengshuang
# created: 2022年11月30日
# desc: 该框架运行脚本用于SaMD的表单运行模式
# 
# 脚本使用具名参数，约定需要传入的参数说明如下，请确保传入有效值。
# --in pmemd的输入文件。
# --prmtop 参数和拓扑文件。
# --rst 坐标文件。
# --output_dir 为输出结果的目录。


for key in "$@"; do
  case $key in
    --in=*)
      in="${key#*=}"
      shift # past argument=value
      ;;
   --prmtop=*)
      prmtop="${key#*=}"
      shift # past argument=value
      ;;
   --rst=*)
      rst="${key#*=}"
      shift # past argument=value
      ;;
   --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
   *)
      ;;
  esac
done


if [ -z $in ] || [ ! -f $in ]; then
  echo "please input *.in"
  exit 1
fi

if [ -z $prmtop ] || [ ! -f $prmtop ]; then
  echo "please input *.prmtop"
  exit 1
fi

if [ -z $rst ] || [ ! -f $rst ]; then
  echo "please input *.rst"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

# 创建输出目录
output_dir=${output_dir%*/}
mkdir -p $output_dir

# 输出运行模式
echo "mode: bash run.sh"

input_f1=$in
input_f2=$prmtop
input_f3=$rst

source ~/.bashrc

# 打印输出文件的路径
echo $output_dir/$(basename $input_f1 .in).out
echo $output_dir/$(basename $input_f1 .in).ncrst
echo $output_dir/$(basename $input_f1 .in).nc
echo $output_dir/$(basename $input_f1 .in).mdinfo

/root/amber20/bin/pmemd.cuda -O -i $input_f1 -o $output_dir/$(basename $input_f1 .in).out -p $input_f2 -c $input_f3 -r $output_dir/$(basename $input_f1 .in).ncrst -x $output_dir/$(basename $input_f1 .in).nc -inf $output_dir/$(basename $input_f1 .in).mdinfo &
sleep 5
# pmemd.cuda的运行时间较长，为了方便监控输出文件的变化
PID=$(pidof "pmemd.cuda")
echo "pmemd.cuda PID"
echo $PID
tail -f $output_dir/$(basename $input_f1 .in).out --pid=$PID

