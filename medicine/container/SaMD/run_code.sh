# !/bin/bash

# author: pengshuang
# created: 2022年11月30日
# desc: 该框架运行脚本用于SaMD的代码运行模式
# 
# 脚本使用具名参数，约定需要传入的参数说明如下，请确保传入有效值。
# --in pmemd的输入文件。
# --prmtop 参数和拓扑文件。
# --rst 坐标文件。


# output_dir为调度侧传入的参数
output_dir=${output_dir%*/}
mkdir -p $output_dir

# 输出运行模式
echo "mode: command-line"

# 示例输入文件的地址
input_f1=/dros/common/ecosystem/MEDICINE_COMPUTING/apps/SaMD/EXAMPLE/SaMD_CT/04_Prod.in
input_f2=/dros/common/ecosystem/MEDICINE_COMPUTING/apps/SaMD/EXAMPLE/SaMD_CT/complex_solv.leap.prmtop
input_f3=/dros/common/ecosystem/MEDICINE_COMPUTING/apps/SaMD/EXAMPLE/SaMD_CT/03_Equil3NPT.rst

# 打印输入文件的路径
echo $input_f1
echo $input_f2
echo $input_f3
# 打印输出文件的路径
echo $output_dir/$(basename $input_f1 .in).out
echo $output_dir/$(basename $input_f1 .in).ncrst
echo $output_dir/$(basename $input_f1 .in).nc
echo $output_dir/$(basename $input_f1 .in).mdinfo

source ~/.bashrc
/root/amber20/bin/pmemd.cuda -O -i $input_f1 -o $output_dir/$(basename $input_f1 .in).out -p $input_f2 -c $input_f3 -r $output_dir/$(basename $input_f1 .in).ncrst -x $output_dir/$(basename $input_f1 .in).nc -inf $output_dir/$(basename $input_f1 .in).mdinfo &
sleep 5
# pmemd.cuda的运行时间较长，为了方便监控输出文件的变化
PID=$(pidof "pmemd.cuda")
echo "pmemd.cuda PID"
echo $PID
tail -f $output_dir/$(basename $input_f1 .in).out --pid=$PID
