#!/bin/bash
# author: sunning
# created: 2022年12月8日18:24:00
# updated: 2022年12月9日11:29:44
# desc: 该脚本用于FastFold预测蛋白质结构作业多链模板运行脚本(1220版本，模型路径直接固定，不再接收)
#       所用数据集路径为启pod时默认挂的oss路径
# 该脚本接收7个参数
# --input_path 待预测蛋白质序列fasta文件所在路径
# --output_dir 结果输出目录
# --gpus 该作业使用的gpu卡数
# --height 结果可视化图片的高度
# --width 结果可视化图片的宽度
# --dpi 结果可视化图片的分辨率
height=1500
width=2000
dpi=1200
for key in "$@"; do
  case $key in
    --input_path=*)
      input_path="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    --model_path=*)
      param_path="${key#*=}"
      shift # past argument=value
      ;;
    --height=*)
      height="${key#*=}"
      shift # past argument=value
      ;;
    --width=*)
      width="${key#*=}"
      shift # past argument=value
      ;;
    --dpi=*)
      dpi="${key#*=}"
      shift # past argument=value
      ;;
    --log_dir=*)
      log_dir="${key#*=}"
      shift # past argument=value
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value
      ;;
    *)
      ;;
  esac
done

if [ -z $input_path ] ; then
  echo "please input fasta file path"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

source activate fastfold
cd /home/fastfold2/FastFold/
python inference.py $input_path /dros/common/public/dataset/pdb_mmcif/mmcif_files \
    --output_dir $output_dir \
    --gpus $gpus \
    --uniref90_database_path /dros/common/public/dataset/uniref90/uniref90.fasta \
    --mgnify_database_path /dros/common/public/dataset/mgnify/mgy_clusters_2018_12.fa \
    --pdb70_database_path /dros/common/public/dataset/pdb70/pdb70 \
    --pdb_seqres_database_path /dros/common/public/dataset/uniprot/pdb_seqres.txt \
    --uniprot_database_path /dros/common/public/dataset/uniprot/uniprot.fasta \
    --uniclust30_database_path /dros/common/public/dataset/uniclust30/uniclust30_2018_08/uniclust30_2018_08 \
    --bfd_database_path /dros/common/public/dataset/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
    --jackhmmer_binary_path `which jackhmmer` \
    --hhblits_binary_path `which hhblits` \
    --hhsearch_binary_path `which hhsearch` \
    --kalign_binary_path `which kalign`  \
    --model_preset multimer \
    --param_path /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/MODEL/params_model_5_multimer_v2.npz \
    --model_name model_5_multimer \
    --enable_workflow &&

conda deactivate
python /opt/pymol/lib/python/pdb2png.py $output_dir $output_dir $height $width $dpi