#!/bin/bash

# author: sunning
# created: 2022年12月6日14:44:00
# updated: 2022年12月6日14:44:44
# desc: 该脚本用于FastFold预测蛋白质结构作业通用模板运行脚本(1220版本，模型路径直接固定，不再接收)
#       所用数据集路径为启pod时默认挂的oss路径
# 该脚本接收7个参数
# --input_dir 待预测蛋白质序列fasta文件所在文件夹
# --output_dir 结果输出目录
# --gpus 该作业使用的gpu卡数
# --height 结果可视化图片的高度
# --width 结果可视化图片的宽度
# --dpi 结果可视化图片的分辨率

height=1500
width=2000
dpi=1200
for key in "$@"; do
  case $key in
    --input_dir=*)
      input_dir="${key#*=}"
      shift # past argument=value
      ;;
    --output_dir=*)
      output_dir="${key#*=}"
      shift # past argument=value
      ;;
    --gpus=*)
      gpus="${key#*=}"
      shift # past argument=value
      ;;
    --model_path=*)
      param_path="${key#*=}"
      shift # past argument=value
      ;;
    --height=*)
      height="${key#*=}"
      shift # past argument=value
      ;;
    --width=*)
      width="${key#*=}"
      shift # past argument=value
      ;;
    --dpi=*)
      dpi="${key#*=}"
      shift # past argument=value
      ;;
    --log_dir=*)
      log_dir="${key#*=}"
      shift # past argument=value
      ;;
    --default)
      DEFAULT=YES
      shift # past argument with no value
      ;;
    *)
      ;;
  esac
done

if [ -z $input_dir ] || [ ! -d $input_dir ]; then
  echo "please input data directory containing fasta file"
  exit 1
fi

if [ -z $output_dir ] ; then
  echo "please input valid output_dir!"
  exit 1
fi

source activate fastfold
python inference1.py  \
    --input_dir $input_dir \
    --template_mmcif_dir /dros/common/public/dataset/pdb_mmcif/mmcif_files/ \
    --output_dir $output_dir \
    --gpus $gpus \
    --param_path /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/MODEL/params_model_5.npz \
    --use_precomputed_alignments /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/DATA/alignments \
    --uniref90_database_path /dros/common/public/dataset/uniref90/uniref90.fasta \
    --mgnify_database_path /dros/common/public/dataset/mgnify/mgy_clusters_2018_12.fa \
    --pdb70_database_path /dros/common/public/dataset/pdb70/pdb70 \
    --uniclust30_database_path /dros/common/public/dataset/uniclust30/uniclust30_2018_08/uniclust30_2018_08 \
    --bfd_database_path /dros/common/public/dataset/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
    --jackhmmer_binary_path `which jackhmmer` \
    --hhblits_binary_path `which hhblits` \
    --hhsearch_binary_path `which hhsearch` \
    --kalign_binary_path `which kalign` && 

conda deactivate
python /opt/pymol/lib/python/pdb2png.py $output_dir $output_dir $height $width $dpi
