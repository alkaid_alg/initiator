#!/bin/bash
height=1500
width=2000
dpi=1200

source activate fastfold
cd /home/fastfold2/FastFold/
python inference.py /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/EXAMPLE/FastFoldProteinStructure_multimer/target.fasta /dros/common/public/dataset/pdb_mmcif/mmcif_files \
    --output_dir $output_dir \
    --gpus $gpus \
    --uniref90_database_path /dros/common/public/dataset/uniref90/uniref90.fasta \
    --mgnify_database_path /dros/common/public/dataset/mgnify/mgy_clusters_2018_12.fa \
    --pdb70_database_path /dros/common/public/dataset/pdb70/pdb70 \
    --pdb_seqres_database_path /dros/common/public/dataset/uniprot/pdb_seqres.txt \
    --uniprot_database_path /dros/common/public/dataset/uniprot/uniprot.fasta \
    --uniclust30_database_path /dros/common/public/dataset/uniclust30/uniclust30_2018_08/uniclust30_2018_08 \
    --bfd_database_path /dros/common/public/dataset/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
    --jackhmmer_binary_path `which jackhmmer` \
    --hhblits_binary_path `which hhblits` \
    --hhsearch_binary_path `which hhsearch` \
    --kalign_binary_path `which kalign`  \
    --model_preset multimer \
    --param_path /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/MODEL/params_model_5_multimer_v2.npz \
    --model_name model_5_multimer \
    --enable_workflow &&

conda deactivate
python /opt/pymol/lib/python/pdb2png.py $output_dir $output_dir $height $width $dpi