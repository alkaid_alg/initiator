#!/bin/bash

# author: sunning
# created: 2022年11月17日10:20:18
# updated: 2022年11月29日18:50:44
# desc: 该脚本用于FastFold预测蛋白质结构作业代码模式的示例运行脚本
#       所用数据集路径为启pod时默认挂的oss路径，输入文件路径为示例文件所在路径
# 该脚本接收8个参数，由环境变量传入
# --output_dir 结果输出目录
# --gpus 该作业使用的gpu卡数
# --model_path 该模型使用的模型参数路径
# --height 结果可视化图片的高度
# --width 结果可视化图片的宽度
# --dpi 结果可视化图片的分辨率

height=1500
width=2000
dpi=1200

source activate fastfold
python inference1.py  \
  --input_dir /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/EXAMPLE/FastFoldProteinStructure_multimer/input_dir/\
  --output_dir $output_dir \
  --gpus $gpus \
  --param_path  $model_path\
  --template_mmcif_dir /dros/common/public/dataset/pdb_mmcif/mmcif_files/ \
  --use_precomputed_alignments /dros/common/ecosystem/MEDICINE_COMPUTING/apps/FastFoldProteinStructure/DATA/alignments \
  --uniref90_database_path /dros/common/public/dataset/uniref90/uniref90.fasta \
  --mgnify_database_path /dros/common/public/dataset/mgnify/mgy_clusters_2018_12.fa \
  --pdb70_database_path /dros/common/public/dataset/pdb70/pdb70 \
  --uniclust30_database_path /dros/common/public/dataset/uniclust30/uniclust30_2018_08/uniclust30_2018_08 \
  --bfd_database_path /dros/common/public/dataset/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
#  --jackhmmer_binary_path ` + "`" + `which jackhmmer` + "`" + `\
  --jackhmmer_binary_path `which jackhmmer` \
#  --hhblits_binary_path ` + "`" + `which hhblits` + "`" + ` \
  --hhblits_binary_path `which hhblits` \
#  --hhsearch_binary_path ` + "`" + `which hhsearch` + "`" + ` \
  --hhsearch_binary_path `which hhsearch` \
#  --kalign_binary_path ` + "`" + `which kalign` + "`" + ` &&
  --kalign_binary_path `which kalign`  &&

conda deactivate
python /opt/pymol/lib/python/pdb2png.py $output_dir $output_dir $height $width $dpi
